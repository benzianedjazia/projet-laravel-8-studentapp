<?php

use App\Http\Controllers\ChartController;
use App\Http\Controllers\chartControllers;
use App\Http\Controllers\classeControllers;
use App\Http\Controllers\EtudiantControllers;
use App\Http\Controllers\statisticsControllers;
use App\Http\Controllers\StudentController;
use App\Models\classes;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('Accueil');

Route::get('/ex',[ChartController::class,'chart2'])->name('ex');
Route::get('/chart',[StudentController::class,'index'])->name('chart');
Route::get('/chart_ex',[ChartController::class,'index'])->name('chart_ex');
Route::get('/statistics',[statisticsControllers::class,'index'])->name('statistics');
 Route::get('/Etudiant',[EtudiantControllers::class,'index'])->name('Etudiant');

 Route::get('/Etudiant/create',[EtudiantControllers::class,'create'])->name('Etudiant.create');
 Route::post('/Etudiant/create',[EtudiantControllers::class,'store'])->name('Etudiant.ajoute');
 Route::delete('/Etudiant/{etudiant}',[EtudiantControllers::class,'delete'])->name('Etudiant.supprimer');

 Route::put('/Etudiant/{etudiant}',[EtudiantControllers::class,'Update'])->name('Etudiant.Update');

 Route::get('/Etudiant/{etudiant}',[EtudiantControllers::class,'edit'])->name('Etudiant.edit');

//  Route::get('/Etudiant',[EtudiantControllers::class])->name('Etudiant');

// Route::get('/Etudiant', function () {
//     $classe= classes::all();
//     return view('Etudiant',compact('classe'));
// })->name('Etudiant');
