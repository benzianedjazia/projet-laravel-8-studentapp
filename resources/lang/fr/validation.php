<?php
return[
    'custom' => [
        'name' => [
            'required' => 'le champ nom est obligatoire',

        ],
        'prenom' => [
            'required' => 'le champ prénom est obligatoire',

        ],
        'classe_id' => [
            'required' => 'le champ classe est obligatoire',

        ],
    ],
];
