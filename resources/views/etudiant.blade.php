@extends('layouts.master')
{{-- use  Illuminate\Database\Eloquent\Collection::links; --}}

 {{--  $(document).ready( function () {
     $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });

     $('#classe_id').on("change",function(e){
        console.log("#DEBUG ")
        loadClass($(this).children("option:selected").val());
        console.log("#DEBUG END")
     });

     function loadClass(class_id){
      url = 'http://localhost/ecole/public/admin/class_students/'+class_id;
        $.ajax({
            url:url,
            method:"GET"
          }).done(function(response){
            console.log(response);
          });
     }

  $('#laravel_datatable').DataTable({
         processing: true,
         serverSide: true,
         ajax: {
          url: "{{ url('users-list') }}",
          type: 'GET',
          data: function (d) {
          d.classe_id = $('#classe_id').val();

        d.mat
          iere_id = $('#matiere_id').val();

          }
         },
         columns: [
                  { data: 'id', name: 'id' },
                  { data: 'nom', name: 'nom' },

               ]
      });
   });

  $('#btnFiterSubmitSearch').click(function(){
     $'#laravel_datatable').DataTable().draw(true);
  });


</script>
  --}}

{{--  <section class="wrapper">
        <div class="table-title">
                <div class="row">
                    <div class="col-sm-6">
                 h2>Gérér <b></b>notes</h2>
          </div>
          <div class="col-sm-6">


            <a href="#addEmployeeModal"class=" btn btn-success" data-toggle="modal"><i class="material icons">&#xE147;</i> <span>Ajouter un nouveau note</span></a>

          </div>


          <div class="col-sm-3">

            <div class="form-group">
              <div class="controls">
              <select name="matiere_id" class="form-control filter-select" required>
                <option value="">--selectionner la matiere svp --</option>
              @foreach($matieres as $matiere)
                  <option value="{{ $matiere->id }}">{{ $matiere->nom_matiere }} </option>
              @endforeach
            </select>
           </div>
            </div>


            <div class="controls">
      <select data-column="0" name="classe_id" id="classe_id"  class="form-control" required>
                <option value="">--selectionner le classe avant svp --</option>
              @foreach($classes as $classe)
                  <option value="{{ $classe->id }}">{{ $classe->classe }} </option>
              @endforeach
            </select>

     </div>
            <button type="text" id="btnFiterSubmitSearch" class="btn btn-info">Submit</button>
          </div>

         <div class="row mt">
          @if(session()->has('success'))
            <div class="alert alert-success">
              {{session()->get('success')}}
            </div>
            @endif
        <div class="col-lg-12">
            <div class="content-panel">

              <section id="no-more-tables">
                <table class="table table-bordered table-striped table-condensed cf">
                  <thead class="cf">
                    <tr>
                    <th>nom eleve </th>
                      <th>prenom</th>
                      <th>nom classe</th>
                     <th>la note  </th>
                      <th>la matiere </th>

                    </tr>
                  </thead>
                  <tbody>


                  @foreach($eleves as $eleve)
                  <tr>

                      <td class="numeric"  data-title="id-note" >{{$eleve->nom}}</td>
                      <td class="numeric"  data-title="Nom">{{$eleve->prenom}}</td>
                      <td class="numeric"  data-title="Nom">{{$eleve->classe->classe}}</td>

                      <td class="numeric"  data-title="Nom">

                     <input  type="text" value ="mettez la note ">

                      </td>
                      <td class="numeric"  data-title="Nom">
                          @foreach($eleve->matieres as $matiere)
                          {{$matiere->nom_matiere}}
                          @endforeach
                      </td>
                    </tr>
                  </tbody>
                  @endforeach

           ``</table>
                <div class="text-center">

            </div>  --}}

@section('contenu')
    <div id="block" class="my-3 p-3 bg-body rounded shadow-sm">
        <h3 class="border-bottom pb-2 mb-4">liste des etudiant </h3>
        <div class="mt-4">

            <div class="d-flex justify-content-end mt-6"> <a href="{{ route('Etudiant.create') }}" class="btn btn-info">Ajouter
                    un nouvel Etudiant</a>
            </div>


            @if (session()->has('successDelete'))
                <div class="alert alert-success">
                    <h3>{{ session()->get('successDelete') }}</h3>
                </div>
            @endif
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <table class="table table-bordered table-hover ">


                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">nom</th>
                        <th scope="col">prenom</th>
                        <th scope="col">classe</th>
                        <th scope="col">action</th>
                    </tr>
                    {{-- <tr> <th scope="col">classe</th></tr> --}}
                </thead>
                <tbody>
                    @foreach ($etudiants as $etudiant)
                        <tr>

                            <th scope="row">{{ $loop->index + 1 }}</th>
                            <td>{{ $etudiant->name }}</td>
                            <td>{{ $etudiant->prenom }}</td>

                            <td>{{ $etudiant->classe->libelle }}</td>



                            <td><a href="{{ route('Etudiant.edit', ['etudiant' => $etudiant->id]) }}"
                                    class="btn btn-info">Editer</a>

                                <a href="#" class="btn btn-danger"
                                    onclick="if(confirm('voules-vous vraiment supprimer cet étudiant?')) {document.getElementById('form-{{ $etudiant->id }}').submit()}">Supprimer</a>
                                <form id="form-{{ $etudiant->id }}"
                                    action="{{ route('Etudiant.supprimer', ['etudiant' => $etudiant->id]) }}" method="post">

                                    @csrf
                                    <input type="hidden" name="_method" value="delete">


                                </form>
                            </td>

                        </tr>
                    @endforeach

                </tbody>
                {{ $etudiants->links() }}

            </table>
        </div>

    </div>
    </div>

    {{-- qezrsdtgyhujklm --}}



@endsection
