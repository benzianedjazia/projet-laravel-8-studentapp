<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Hugo 0.98.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous">
    </script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <title>STUDENT’APP</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/5.2/examples/offcanvas-navbar/">
    <!-- Favicon -->
    <link rel="icon" type="image/x-icon" href="../assets/img/favicon/favicon.ico" />


    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
     rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
         crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
        crossorigin="anonymous">
    </script>


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link href="{{ url('css/css.css') }}" rel="stylesheet" />
    <link rel="manifest" href="{{ url('css/manifest.json') }}">

    <script src="{{ url('js/OverlayScrollbars.min.js') }}"></script>
    <script src="{{ url('js/config.js') }}"></script>
    <link href="{{ url('css/OverlayScrollbars.min.css') }}" rel="stylesheet">

    {{--  <link href="{{ url('css/theme-rtl.min.css') }}" rel="stylesheet" id="style-rtl">
    <link href="{{ url('css/theme.min.css') }}" rel="stylesheet" id="style-default">
    <link href="{{ url('css/user-rtl.min.css') }}" rel="stylesheet" id="user-style-rtl">
    <link href="{{ url('css/user.min.css') }}" rel="stylesheet" id="user-style-default">  --}}

    <link rel="stylesheet" href="{{ url('css/feather.css') }}">
    <link rel="stylesheet" href="{{ url('css/materialdesignicons.min.css"') }}>
  <link rel="stylesheet"
        href="{{ url('css/themify-icons.css') }}">
    <link rel="stylesheet" href="{{ url('css/typicons.css') }}">

    <link rel="stylesheet" href="{{ url('css/chartist.min.css') }}">
    <link rel="stylesheet" href="{{ url('css/main.3642ce7b.css') }}">
    <link rel="stylesheet" href="{{ url('css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ url('css/simple-line-icons.css') }}">
    <link rel="stylesheet" href="{{ url('css/vendor.bundle.base.css') }}">
    <!-- endinject -->
    <!-- Plugin css for this page -->
    <link rel="stylesheet" href="{{ url('css/dataTables.bootstrap4.css') }}">
    <link rel="stylesheet" href="{{ url('css/select.dataTables.min.css') }}">
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    {{--  <link rel="stylesheet" href="{{ url('css/style1.css') }}">  --}}
    <!-- endinject -->
    <link rel="shortcut icon" href="images/favicon.png" />
    <!-- Icons. Uncomment required icon fonts -->
    <link rel="stylesheet" href=" {{ url('css/boxicons.css') }}" />

    <!-- Core CSS -->
    <link rel="stylesheet" href="  {{ url('css/core.css') }}" class="template-customizer-core-css" />
    <link rel="stylesheet" href="{{ url('css/theme-default.css') }}" class="template-customizer-theme-css" />
    <link rel="stylesheet" href="{{ url('css/demo.css') }}" />

    <!-- Vendors CSS -->
    <link rel="stylesheet" href="{{ url('libs/perfect-scrollbar/perfect-scrollbar.css') }}" />

    <link rel="stylesheet" href="{{ url('libs/apex-charts/apex-charts.css') }}" />

    <!-- Page CSS -->

    <!-- Helpers -->
    <script src="{{ url('js/helpers.js') }}"></script>

    <!--! Template customizer & Theme config files MUST be included after core stylesheets and helpers.js in the <head> section -->
    <!--? Config:  Mandatory theme config file contain global vars & default theme options, Set your preferred theme option in this file.  -->
    <script src="{{ url('js/config.js') }}"></script>








    <link rel="stylesheet" href="{{ url('css/flag-icon.min.css') }}" />
    <link rel="stylesheet" href="{{ url('css/vendor.bundle.base.css') }}" />
    <link rel="stylesheet" href="{{ url('css/bootstrap-datepicker.min.css') }}" />
    {{--  <link rel="stylesheet" href="{{ url('css/style1.css') }}" />  --}}
    {{--  <link rel="stylesheet" href="{{ url('css/dashbord.scss') }}" />  --}}
















</head>
<style>
    {{--  .layout-navbar{
    transform: translateX(-244px) !important ;
  }   --}} .block1 {
        border-bottom: 2px solid;
    }
</style>




<link href="../../public/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">

<!-- Favicons -->
<link rel="apple-touch-icon" href="/docs/5.2/assets/img/favicons/apple-touch-icon.png" sizes="180x180">
<link rel="icon" href="/docs/5.2/assets/img/favicons/favicon-32x32.png" sizes="32x32" type="image/png">
<link rel="icon" href="/docs/5.2/assets/img/favicons/favicon-16x16.png" sizes="16x16" type="image/png">
<link rel="manifest" href="/docs/5.2/assets/img/favicons/manifest.json">
<link rel="mask-icon" href="/docs/5.2/assets/img/favicons/safari-pinned-tab.svg" color="#712cf9">
<link rel="icon" href="/docs/5.2/assets/img/favicons/favicon.ico">
<meta name="theme-color" content="#712cf9">


<style>
    .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        user-select: none;
    }

    @media (min-width: 768px) {
        .bd-placeholder-img-lg {
            font-size: 3.5rem;
        }
    }

    .b-example-divider {
        height: 3rem;
        background-color: rgba(0, 0, 0, .1);
        border: solid rgba(0, 0, 0, .15);
        border-width: 1px 0;
        box-shadow: inset 0 .5em 1.5em rgba(0, 0, 0, .1), inset 0 .125em .5em rgba(0, 0, 0, .15);
    }

    .b-example-vr {
        flex-shrink: 0;
        width: 1.5rem;
        height: 100vh;
    }

    .bi {
        vertical-align: -.125em;
        fill: currentColor;
    }

    .nav-scroller {
        position: relative;
        z-index: 2;
        height: 2.75rem;
        overflow-y: hidden;
    }

    .nav-scroller .nav {
        display: flex;
        flex-wrap: nowrap;
        padding-bottom: 1rem;
        margin-top: -1px;
        overflow-x: auto;
        text-align: center;
        white-space: nowrap;
        -webkit-overflow-scrolling: touch;
    }

    #block {
        transform: translateY(237px);
    }
</style>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"
    integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.bundle.min.js"
    integrity="sha512-vBmx0N/uQOXznm/Nbkp7h0P1RfLSj0HQrFSzV8m7rOGyj30fYAOKHYvCNez+yM8IrfnW0TCodDEjRqf6fodf/Q=="
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>



<!-- Custom styles for this template -->
<link href="../../public/css/offcanvas.css" rel="stylesheet">
</head>

<body class="bg-light">

    <nav class="navbar navbar-expand-lg fixed-top navbar-dark bg-dark" aria-label="Main navigation">
        <div class="container-fluid">
            <a class="navbar-brand" href="#"
                style=" font-size: large ;   font-kerning: auto;
    font-size: 30px;color:red; font-style:italic;font-weight:bold;">STUDENT’APP</a>
            <button class="navbar-toggler p-0 border-0" type="button" id="navbarSideCollapse"
                aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="navbar-collapse offcanvas-collapse" id="navbarsExampleDefault">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link " aria-current="page" href="{{ route('Accueil') }}">Accueil</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" href="{{ route('Etudiant') }}">Etudiant</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link " href="{{ route('chart') }}">chart</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link " href="{{ route('statistics') }}">statistics</a>
                    </li>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link " href="{{ route('chart_ex') }}">chart_ex</a>
                    </li>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link " href="{{ route('ex') }}">ex</a>
                    </li>
                    <!-- <li class="nav-item">
                        <a class="nav-link" href="#">Profile</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Switch account</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-bs-toggle="dropdown" aria-expanded="false">Settings</a>
                        <ul class="dropdown-menu" aria-labelledby="dropdown01">
                            <li><a class="dropdown-item" href="#">Action</a></li>
                            <li><a class="dropdown-item" href="#">Another action</a></li>
                            <li><a class="dropdown-item" href="#">Something else here</a></li>
                        </ul>
                    </li> -->
                </ul>
                <form class="d-flex" role="search">
                    <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
                    <button class="btn btn-outline-success" type="submit">Search</button>
                </form>
            </div>
        </div>
    </nav>



    <main class="container">


        @yield('contenu')



    </main>
    <!-- endinject -->
    <!-- Plugin js for this page -->
    <script src="{{ url('js/jquery.flot.js') }}"></script>


    <script src="{{ url('js/jquery.flot.resize.js') }}"></script>
    <script src="{{ url('js/jquery.flot.categories.js') }}"></script>
    <script src="{{ url('js/jquery.flot.fillbetween.js') }}"></script>
    <script src="{{ url('js/jquery.flot.stack.js') }}"></script>
    <script src="{{ url('js/jquery.flot.pie.js') }}"></script>
    <!-- End plugin js for this page -->
    <!-- inject:js -->
    <script src="{{ url('js/misc.js') }}"></script>
    <!-- endinject -->
    <!-- Custom js for this page -->








    <script src="{{ url('js/canvasjs.min.js') }}"></script>



    <!-- Core JS -->
    <script src="{{ url('js/popper.min.js') }}"></script>
    <script src="{{ url('js/anchor.min.js') }}"></script>
    <script src="{{ url('js/bootstrap.min.js') }}"></script>
    <script src="{{ url('js/is.min.js') }}"></script>
    <script src="{{ url('js/chart.min.js') }}"></script>
    <script src="{{ url('js/countUp.umd.js') }}"></script>
    <script src="{{ url('js/lodash.min.js') }}"></script>
    <script src="{{ url('js/echarts.min.js') }}"></script>
    <script src="{{ url('js/dayjs.min.js') }}"></script>
    <script src="{{ url('js/all.min.js') }}"></script>
    <script src="{{ url('js/lodash.min.js') }}"></script>
    <script src="{{ url('js/polyfill.min.js') }}"></script>
    <script src="{{ url('js/list.min.js') }}"></script>
    <script src="{{ url('js/theme.js') }}"></script>
    <!-- build:js assets/vendor/js/core.js -->
    <script src="{{ url('libs/jquery/jquery.js') }}"></script>
    <script src="{{ url('libs/popper/popper.js') }}"></script>
    <script src="{{ url('js/bootstrap.js') }}"></script>
    <script src="{{ url('libs/perfect-scrollbar/perfect-scrollbar.js') }}"></script>

    <script src="{{ url('js/menu.js') }}"></script>
    <!-- endbuild -->

    <!-- Vendors JS -->
    <script src="{{ url('libs/apex-charts/apexcharts.js') }}"></script>

    <!-- Main JS -->
    <script src="{{ url('js/main.js') }}"></script>

    <!-- Page JS -->
    <script src="{{ url('js/dashboards-analytics.js') }}"></script>

    <!-- Place this tag in your head or just before your close body tag. -->
    <script async defer src="{{ url('js/buttons.js') }}"></script>
    <script src="{{ url('js/vendor.bundle.base.js') }}"></script>
    <!-- endinject -->
    <!-- Plugin js for this page -->
    <script src="{{ url('js/Chart.min.js') }}"></script>
    <script src="{{ url('js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ url('js/progressbar.min.js') }}"></script>

    <!-- End plugin js for this page -->
    <!-- inject:js -->
    <script src="{{ url('js/off-canvas.js') }}"></script>
    <script src="{{ url('js/hoverable-collapse.js') }}"></script>
    <script src="{{ url('js/template.js') }}"></script>
    <script src="{{ url('js/settings.js') }}"></script>
    <script src="{{ url('js/todolist.js') }}"></script>
    <!-- endinject -->
    <!-- Custom js for this page-->
    <script src="{{ url('js/dashboard.js') }}"></script>
    <script src="{{ url('js/chartist.min.js') }}"></script>
    <script src="{{ url('js/main.995f07b9.js') }}"></script>
    <script src="{{ url('js/Chart.roundedBarCharts.js') }}"></script>

    <script src="../../public/js/bootstrap.bundle.min.js"
        integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous">
    </script>

    <script src="../../public/js/offcanvas.js"></script>
</body>

</html>
