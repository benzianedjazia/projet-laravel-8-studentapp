@extends('layouts.master')

@section('contenu')
    <section class="block1">

        <div class="container-scroller">

            <!-- partial -->
            <div class="container-fluid page-body-wrapper">
                <!-- partial:partials/_settings-panel.html -->

                <!-- partial -->
                <div class="main-panel">
                    <div class="content-wrapper">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="home-tab">
                                    <div class="d-sm-flex align-items-center justify-content-between border-bottom">
                                        <ul class="nav nav-tabs" role="tablist">
                                            <li class="nav-item">
                                                <a class="nav-link active ps-0" id="home-tab" data-bs-toggle="tab"
                                                    href="#overview" role="tab" aria-controls="overview"
                                                    aria-selected="true">Overview</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" id="profile-tab" data-bs-toggle="tab" href="#audiences"
                                                    role="tab" aria-selected="false">Audiences</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" id="contact-tab" data-bs-toggle="tab"
                                                    href="#demographics" role="tab"
                                                    aria-selected="false">Demographics</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link border-0" id="more-tab" data-bs-toggle="tab"
                                                    href="#more" role="tab" aria-selected="false">More</a>
                                            </li>
                                        </ul>
                                        <div>
                                            <div class="btn-wrapper">
                                                <a href="#" class="btn btn-otline-dark align-items-center"><i
                                                        class="icon-share"></i> Share</a>
                                                <a href="#" class="btn btn-otline-dark"><i class="icon-printer"></i>
                                                    Print</a>
                                                <a href="#" class="btn btn-primary text-white me-0"><i
                                                        class="icon-download"></i> Export</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-content tab-content-basic">
                                        <div class="tab-pane fade show active" id="overview" role="tabpanel"
                                            aria-labelledby="overview">
                                            <div class="row">




                                                <div class="col-sm-12">
                                                    <div
                                                        class="statistics-details d-flex align-items-center justify-content-between">
                                                        <div>
                                                            <p class="statistics-title">nombre etudiant</p>


                                                             <h3 class="rate-percentage">{{ $req1 }}</h3>
                                                            <p class="text-danger d-flex"><i
                                                                    class="mdi mdi-menu-down"></i><span>-0.5%</span></p>
                                                        </div>
                                                        <div>
                                                            <p class="statistics-title">classe Terminale </p>
                                                            <h3 class="rate-percentage">{{ $req2 }}</h3>
                                                            <p class="text-success d-flex"><i
                                                                    class="mdi mdi-menu-up"></i><span>+0.1%</span></p>
                                                        </div>
                                                        <div>
                                                            <p class="statistics-title">classe Permiere </p>
                                                            <h3 class="rate-percentage">{{ $req3 }}</h3>
                                                            <p class="text-danger d-flex"><i
                                                                    class="mdi mdi-menu-down"></i><span>68.8</span></p>
                                                        </div>
                                                        <div class="d-none d-md-block">
                                                             <p class="statistics-title">classe Seconde </p>
                                                            <h3 class="rate-percentage">{{ $req4 }}</h3>
                                                            <p class="text-success d-flex"><i
                                                                    class="mdi mdi-menu-down"></i><span>+0.8%</span></p>
                                                        </div>
                                                        <div class="d-none d-md-block">
                                                                        <p class="statistics-title">classe 3eme </p>
                                                            <h3 class="rate-percentage">{{ $req5 }}</h3>
                                                            <p class="text-danger d-flex"><i
                                                                    class="mdi mdi-menu-down"></i><span>68.8</span></p>
                                                        </div>
                                                        <div class="d-none d-md-block">
                                                                     <p class="statistics-title">classe 4eme </p>
                                                            <h3 class="rate-percentage">{{ $req6 }}</h3>
                                                            <p class="text-success d-flex"><i
                                                                    class="mdi mdi-menu-down"></i><span>+0.8%</span></p>
                                                        </div>
                                                        <div class="d-none d-md-block">
                                                                     <p class="statistics-title">classe 5eme </p>
                                                            <h3 class="rate-percentage">{{ $req7 }}</h3>
                                                            <p class="text-success d-flex"><i
                                                                    class="mdi mdi-menu-down"></i><span>+0.8%</span></p>
                                                        </div>
                                                        <div class="d-none d-md-block">
                                                                     <p class="statistics-title">classe 6eme </p>
                                                            <h3 class="rate-percentage">{{ $req8 }}</h3>
                                                            <p class="text-success d-flex"><i
                                                                    class="mdi mdi-menu-down"></i><span>+0.8%</span></p>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-8 d-flex flex-column">
                                                    <div class="row flex-grow">
                                                        <div class="col-12 col-lg-4 col-lg-12 grid-margin stretch-card">
                                                            <div class="card card-rounded">
                                                                <div class="card-body">
                                                                    <div
                                                                        class="d-sm-flex justify-content-between align-items-start">
                                                                        <div>
                                                                            <h4 class="card-title card-title-dash">
                                                                                Performance Line Chart</h4>
                                                                            <h5 class="card-subtitle card-subtitle-dash">
                                                                                Lorem Ipsum is simply dummy text of the
                                                                                printing</h5>
                                                                        </div>
                                                                        <div id="performance-line-legend"></div>
                                                                    </div>
                                                                    <div class="chartjs-wrapper mt-5">
                                                                        <canvas id="performaneLine"></canvas>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4 d-flex flex-column">

                                                        <div class="col-md-6 col-lg-12 grid-margin stretch-card">
                                                            <div class="card card-rounded">
                                                                <div class="card-body">
                                                                    <div class="row">
                                                                        <div class="col-sm-6">
                                                                            <div
                                                                                class="d-flex justify-content-between align-items-center mb-2 mb-sm-0">
                                                                                <div class="circle-progress-width">
                                                                                    <div id="totalVisitors"
                                                                                        class="progressbar-js-circle pr-2">
                                                                                    </div>
                                                                                </div>
                                                                                <div>
                                                                                    <p class="text-small mb-2">Total
                                                                                        Visitors</p>
                                                                                    <h4 class="mb-0 fw-bold">26.80%
                                                                                    </h4>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-6">
                                                                            <div
                                                                                class="d-flex justify-content-between align-items-center">
                                                                                <div class="circle-progress-width">
                                                                                    <div id="visitperday"
                                                                                        class="progressbar-js-circle pr-2">
                                                                                    </div>
                                                                                </div>
                                                                                <div>
                                                                                    <p class="text-small mb-2">Visits
                                                                                        per
                                                                                        day</p>
                                                                                    <h4 class="mb-0 fw-bold">9065</h4>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="row flex-grow">
                                                    <div class="col-12 grid-margin stretch-card">
                                                        <div class="card card-rounded">
                                                            <div class="card-body">
                                                                <div
                                                                    class="d-sm-flex justify-content-between align-items-start">
                                                                    <div>
                                                                        <h4 class="card-title card-title-dash">
                                                                            <font style="vertical-align: inherit;">
                                                                                <font style="vertical-align: inherit;">
                                                                                    Aperçu du marché</font>
                                                                            </font>
                                                                        </h4>
                                                                        <p class="card-subtitle card-subtitle-dash">
                                                                            <font style="vertical-align: inherit;">
                                                                                <font style="vertical-align: inherit;">
                                                                                    Lorem ipsum dolor sit amet
                                                                                    consectetur
                                                                                    adipisicing elit</font>
                                                                            </font>
                                                                        </p>
                                                                    </div>
                                                                    <div>
                                                                        <div class="dropdown">
                                                                            <button
                                                                                class="btn btn-secondary dropdown-toggle toggle-dark btn-lg mb-0 me-0"
                                                                                type="button" id="dropdownMenuButton2"
                                                                                data-bs-toggle="dropdown"
                                                                                aria-haspopup="true"
                                                                                aria-expanded="false">
                                                                                <font style="vertical-align: inherit;">
                                                                                    <font style="vertical-align: inherit;">
                                                                                        Ce mois-ci</font>
                                                                                </font>
                                                                            </button>
                                                                            <div class="dropdown-menu"
                                                                                aria-labelledby="dropdownMenuButton2">
                                                                                <h6 class="dropdown-header">
                                                                                    <font style="vertical-align: inherit;">
                                                                                        <font
                                                                                            style="vertical-align: inherit;">
                                                                                            Réglages</font>
                                                                                    </font>
                                                                                </h6>
                                                                                <a class="dropdown-item" href="#">
                                                                                    <font style="vertical-align: inherit;">
                                                                                        <font
                                                                                            style="vertical-align: inherit;">
                                                                                            Action</font>
                                                                                    </font>
                                                                                </a>
                                                                                <a class="dropdown-item" href="#">
                                                                                    <font style="vertical-align: inherit;">
                                                                                        <font
                                                                                            style="vertical-align: inherit;">
                                                                                            Une autre opération</font>
                                                                                    </font>
                                                                                </a>
                                                                                <a class="dropdown-item" href="#">
                                                                                    <font style="vertical-align: inherit;">
                                                                                        <font
                                                                                            style="vertical-align: inherit;">
                                                                                            Quelque chose d'autre ici
                                                                                        </font>
                                                                                    </font>
                                                                                </a>
                                                                                <div class="dropdown-divider"></div>
                                                                                <a class="dropdown-item" href="#">
                                                                                    <font style="vertical-align: inherit;">
                                                                                        <font
                                                                                            style="vertical-align: inherit;">
                                                                                            Lien séparé</font>
                                                                                    </font>
                                                                                </a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div
                                                                    class="d-sm-flex align-items-center mt-1 justify-content-between">
                                                                    <div
                                                                        class="d-sm-flex align-items-center mt-4 justify-content-between">
                                                                        <h2 class="me-2 fw-bold">
                                                                            <font style="vertical-align: inherit;">
                                                                                <font style="vertical-align: inherit;">
                                                                                    36&nbsp;2531,00&nbsp;$</font>
                                                                            </font>
                                                                        </h2>
                                                                        <h4 class="me-2">
                                                                            <font style="vertical-align: inherit;">
                                                                                <font style="vertical-align: inherit;">
                                                                                    USD
                                                                                </font>
                                                                            </font>
                                                                        </h4>
                                                                        <h4 class="text-success">
                                                                            <font style="vertical-align: inherit;">
                                                                                <font style="vertical-align: inherit;">
                                                                                    (+1,37%)</font>
                                                                            </font>
                                                                        </h4>
                                                                    </div>
                                                                    <div class="me-3">
                                                                        <div id="marketing-overview-legend">
                                                                            <div class="chartjs-legend">
                                                                                <ul>
                                                                                    <li class="text-muted text-small">
                                                                                        <span
                                                                                            style="background-color:#52CDFF"></span>
                                                                                        <font
                                                                                            style="vertical-align: inherit;">
                                                                                            <font
                                                                                                style="vertical-align: inherit;">
                                                                                                La semaine dernière
                                                                                            </font>
                                                                                        </font>
                                                                                    </li>
                                                                                    <li class="text-muted text-small">
                                                                                        <span
                                                                                            style="background-color:#1F3BB3"></span>
                                                                                        <font
                                                                                            style="vertical-align: inherit;">
                                                                                            <font
                                                                                                style="vertical-align: inherit;">
                                                                                                Cette semaine</font>
                                                                                        </font>
                                                                                    </li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="chartjs-bar-wrapper mt-3">
                                                                    <div class="chartjs-size-monitor">
                                                                        <div class="chartjs-size-monitor-expand">
                                                                            <div class=""></div>
                                                                        </div>
                                                                        <div class="chartjs-size-monitor-shrink">
                                                                            <div class=""></div>
                                                                        </div>
                                                                    </div>
                                                                    <canvas id="marketingOverview" width="825"
                                                                        height="150"
                                                                        style="display: block; width: 825px; height: 150px;"
                                                                        class="chartjs-render-monitor"></canvas>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8 d-flex flex-column">
                                                    <div class="row flex-grow">
                                                        <div class="col-md-6 col-lg-6 grid-margin stretch-card">
                                                            <div class="card card-rounded">
                                                                <div class="card-body">
                                                                    <div
                                                                        class="d-flex align-items-center justify-content-between mb-3">
                                                                        <h4 class="card-title card-title-dash">
                                                                            Activities
                                                                        </h4>
                                                                        <p class="mb-0">20 finished, 5 remaining</p>
                                                                    </div>
                                                                    <ul class="bullet-line-list">
                                                                        <li>
                                                                            <div class="d-flex justify-content-between">
                                                                                <div><span class="text-light-green">Ben
                                                                                        Tossell</span> assign you a task
                                                                                </div>
                                                                                <p>Just now</p>
                                                                            </div>
                                                                        </li>
                                                                        <li>
                                                                            <div class="d-flex justify-content-between">
                                                                                <div><span class="text-light-green">Oliver
                                                                                        Noah</span> assign you a task
                                                                                </div>
                                                                                <p>1h</p>
                                                                            </div>
                                                                        </li>
                                                                        <li>
                                                                            <div class="d-flex justify-content-between">
                                                                                <div><span class="text-light-green">Jack
                                                                                        William</span> assign you a task
                                                                                </div>
                                                                                <p>1h</p>
                                                                            </div>
                                                                        </li>
                                                                        <li>
                                                                            <div class="d-flex justify-content-between">
                                                                                <div><span class="text-light-green">Leo
                                                                                        Lucas</span> assign you a task
                                                                                </div>
                                                                                <p>1h</p>
                                                                            </div>
                                                                        </li>
                                                                        <li>
                                                                            <div class="d-flex justify-content-between">
                                                                                <div><span class="text-light-green">Thomas
                                                                                        Henry</span> assign you a task
                                                                                </div>
                                                                                <p>1h</p>
                                                                            </div>
                                                                        </li>
                                                                        <li>
                                                                            <div class="d-flex justify-content-between">
                                                                                <div><span class="text-light-green">Ben
                                                                                        Tossell</span> assign you a task
                                                                                </div>
                                                                                <p>1h</p>
                                                                            </div>
                                                                        </li>
                                                                        <li>
                                                                            <div class="d-flex justify-content-between">
                                                                                <div><span class="text-light-green">Ben
                                                                                        Tossell</span> assign you a task
                                                                                </div>
                                                                                <p>1h</p>
                                                                            </div>
                                                                        </li>
                                                                    </ul>
                                                                    <div class="list align-items-center pt-3">
                                                                        <div class="wrapper w-100">
                                                                            <p class="mb-0">
                                                                                <a href="#"
                                                                                    class="fw-bold text-primary">Show
                                                                                    all
                                                                                    <i
                                                                                        class="mdi mdi-arrow-right ms-2"></i></a>
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4 d-flex flex-column">
                                                    <div class="row flex-grow">
                                                        <div class="col-12 grid-margin stretch-card">
                                                            <div class="card card-rounded">
                                                                <div class="card-body">
                                                                    <div class="row">
                                                                        <div class="col-lg-12">
                                                                            <div
                                                                                class="d-flex justify-content-between align-items-center mb-3">
                                                                                <div>
                                                                                    <h4 class="card-title card-title-dash">
                                                                                        Top Performer</h4>
                                                                                </div>
                                                                            </div>
                                                                            <div class="mt-3">
                                                                                <div
                                                                                    class="wrapper d-flex align-items-center justify-content-between py-2 border-bottom">
                                                                                    <div class="d-flex">
                                                                                        <img class="img-sm rounded-10"
                                                                                            src="images/faces/face1.jpg"
                                                                                            alt="profile">
                                                                                        <div class="wrapper ms-3">
                                                                                            <p class="ms-1 mb-1 fw-bold">
                                                                                                Brandon Washington</p>
                                                                                            <small
                                                                                                class="text-muted mb-0">162543</small>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="text-muted text-small">
                                                                                        1h ago
                                                                                    </div>
                                                                                </div>
                                                                                <div
                                                                                    class="wrapper d-flex align-items-center justify-content-between py-2 border-bottom">
                                                                                    <div class="d-flex">
                                                                                        <img class="img-sm rounded-10"
                                                                                            src="images/faces/face2.jpg"
                                                                                            alt="profile">
                                                                                        <div class="wrapper ms-3">
                                                                                            <p class="ms-1 mb-1 fw-bold">
                                                                                                Wayne Murphy</p>
                                                                                            <small
                                                                                                class="text-muted mb-0">162543</small>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="text-muted text-small">
                                                                                        1h ago
                                                                                    </div>
                                                                                </div>
                                                                                <div
                                                                                    class="wrapper d-flex align-items-center justify-content-between py-2 border-bottom">
                                                                                    <div class="d-flex">
                                                                                        <img class="img-sm rounded-10"
                                                                                            src="images/faces/face3.jpg"
                                                                                            alt="profile">
                                                                                        <div class="wrapper ms-3">
                                                                                            <p class="ms-1 mb-1 fw-bold">
                                                                                                Katherine Butler</p>
                                                                                            <small
                                                                                                class="text-muted mb-0">162543</small>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="text-muted text-small">
                                                                                        1h ago
                                                                                    </div>
                                                                                </div>
                                                                                <div
                                                                                    class="wrapper d-flex align-items-center justify-content-between py-2 border-bottom">
                                                                                    <div class="d-flex">
                                                                                        <img class="img-sm rounded-10"
                                                                                            src="images/faces/face4.jpg"
                                                                                            alt="profile">
                                                                                        <div class="wrapper ms-3">
                                                                                            <p class="ms-1 mb-1 fw-bold">
                                                                                                Matthew Bailey</p>
                                                                                            <small
                                                                                                class="text-muted mb-0">162543</small>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="text-muted text-small">
                                                                                        1h ago
                                                                                    </div>
                                                                                </div>
                                                                                <div
                                                                                    class="wrapper d-flex align-items-center justify-content-between pt-2">
                                                                                    <div class="d-flex">
                                                                                        <img class="img-sm rounded-10"
                                                                                            src="images/faces/face5.jpg"
                                                                                            alt="profile">
                                                                                        <div class="wrapper ms-3">
                                                                                            <p class="ms-1 mb-1 fw-bold">
                                                                                                Rafell John</p>
                                                                                            <small
                                                                                                class="text-muted mb-0">Alaska,
                                                                                                USA</small>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="text-muted text-small">
                                                                                        1h ago
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- content-wrapper ends -->
                    <!-- partial:partials/_footer.html -->

                    <!-- partial -->
                </div>
                <!-- main-panel ends -->
            </div>
            <!-- page-body-wrapper ends -->
        </div>
    </section>
@endsection
