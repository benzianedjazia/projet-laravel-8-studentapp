<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\Collection;
use App\Models\Etudiant;
use App\Models\classes;
use Illuminate\Http\Request;

class EtudiantControllers extends Controller
{



    public function index()
    {

        $etudiants = Etudiant::orderBy("name", "asc")->simplePaginate(5);

        return view('etudiant', compact('etudiants'));
    }

    public function create()
    {
        $classes = classes::all();

        return view('createEtudiant', compact('classes'));
    }
    public function store(request $request)
    {
        $request->validate([
            "name" => "required",
            "prenom" => "required",
            "classe_id" => "required"
        ]);
        //  Etudiant::create($request->all());
        Etudiant::create([
            "name" => $request->name,
            "prenom" => $request->prenom,
            "classe_id" => $request->classe_id,
        ]);

        return back()->with("success", "Etudiant ajouté avec succé!");
    }






    public function delete(Etudiant $etudiant)
    {

        $nom_complet = $etudiant->name . " " . $etudiant->prenom;
        $etudiant->delete();

        return back()->with("successDelete", "l'étudiant'$nom_complet' supprimé avec succé!");
    }









    public function edit(classes $classes, Etudiant $etudiant)
    {
        $classes = classes::all();

        return view('editeEtudiant', compact('etudiant', 'classes'));
    }
    public function Update(request $request, Etudiant $etudiant)
    {
        $request->validate([]);
        $etudiant->Update($request->all());
        return back()->with("success", "Etudiant edité avec succé!");
    }


}
