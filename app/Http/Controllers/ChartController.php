<?php

namespace App\Http\Controllers;

use App\Models\Etudiant;

use App\Models\Unit;
use Illuminate\Http\Request;

class ChartController extends Controller
{

    public function index()
    {
        $units = Unit::all();

        $dataPoints = [];

        foreach ($units as $unit) {

            $dataPoints[] = array(
                "day" => $unit['day'],
                "data" => [
                    intval($unit['term1_marks']),
                    // intval($unit['term2_marks']),
                    // intval($unit['term3_marks']),
                    // intval($unit['term4_marks']),
                ],
            );
        }

        return view("chart_ex", [
            "data" => json_encode($dataPoints),
            "terms" => json_encode(array(
                "Term 1",
                // "Term 2",
                // "Term 3",
                // "Term 4",
            )),
        ]);
    }

    public function chart2()
    {
        $etudiants = Etudiant::all();

        $dataPoints = [];

        foreach ($etudiants as $etudiant) {

            $dataPoints[] = array(
                "name" => $etudiant['name'],
                "data" => [
                    intval($etudiant['classe_id']),
                ],
            );
        }

        return view("chart_ex", [
            "data" => json_encode($dataPoints),
            "terms" => json_encode(array(
                "classe",

            )),
        ]);
    }
}
