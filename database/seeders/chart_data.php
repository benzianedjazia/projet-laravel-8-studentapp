<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
  use Illuminate\Support\Facades\DB;

class chart_data extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach(range(1,10) as $item)
        {
            DB::table('chart_data')->insert([
                ["student_subject"=>"maths"],
                ["student_subject"=>"maths"],
                ["student_subject"=>"maths"],
                ["student_subject"=>"maths"],
                ["student_subject"=>"maths"],
                ["student_subject"=>"maths"],
                ["student_subject"=>"maths"],
                    ],
                [
                    ["student_name"=>"kenza"],
                    ["student_name"=>"djazia"],
                    ["student_name"=>"fatiha"],
                    ["student_name"=>"mima"],
                    ["student_name"=>"aya"],
                    ["student_name"=>"hosine"],
                    ["student_name"=>"nosa"],
                ],
                [
                    ["student_marks"=>"12"],
                    ["student_marks"=>"18"],
                    ["student_marks"=>"45"],
                    ["student_marks"=>"478"],
                    ["student_marks"=>"78"],
                    ["student_marks"=>"23"],
                    ["student_marks"=>"19"],
                ]);

        }
    }
}
