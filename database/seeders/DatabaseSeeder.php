<?php

namespace Database\Seeders;

use App\Models\Etudiant;
use App\Models\Student;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // chart_data::factory(10)->creat();
        Etudiant::factory(10)->create();
        $this->call(ClassesTabeleSeeder::class);
        $this->call(EtudiantSeeder::class);

        $this->call(chart_data::class);
        $this->call(StudentSeeder::class);
    }
}
