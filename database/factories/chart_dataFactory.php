<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class chart_dataFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'student_name'=>$this->faker->name,
            'student_subject'=>$this->faker,
            'student_marks'=>$this->faker,
        ];
    }
}
